
The project is a Java Maven project.
The _./pom.xml_ file contains all the dependencies needed, plus some more such as Tinkerpop for more extendability.
It was developed using ***IntelliJ***, so the configuration for IntellJ should already work.


The BPMN files are located under _data/BPMN20ModelsReprository/_.
Any export files such as GraphML land in _export/_
The source files are located under _src/main/java/org/etothepii_.
The entry method is the main method of the _App.java_.
Running this main method with java is the only way the project can be run, there are no other main methods.


In the _App.java_ is located the logic for reading the BPMN files and calling database adapters to insert processes and submit queries.
The _App.java_ uses a list of database adapters in line ***55***. To adjust the list of used adapters, the commented lines have to be edited.
For the databases, the default ports were used on the host machine. The database names are ones like _mydb_ and _neo4j_ and are ***hardcoded in the connection strings!***
If your database names or username and password deviate, you need to edit the connection string in the adapter.

For making the code run with, e.g. Neo4j, do as follows:
1. Start the Neo4j database on the local machine.
2. Edit the code of the _App.java_ to use only the _Neo4jAdapter_.
3. Optionally, edit the code of _App.java_ to do the Evaluation of Insert/Read queries respectively. Comment lines ***66*** and ***77*** for that.
4. Edit the connection string in the _Neo4jAdapter.java_ to match your database connection and database name. Also your username and password for the database.
5. Run _Main.java_






> Recommended System Requirements:
* \> 2 Gb of RAM
* Storage disk space for graph databases and installed graph databases. For supported ones see the list in the thesis.
* Java 17





