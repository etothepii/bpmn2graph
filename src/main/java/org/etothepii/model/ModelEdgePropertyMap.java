package org.etothepii.model;

import java.util.HashMap;


/**
 * A PropertyMap for Edges, according to the graph database schema of our thesis
 */
public class ModelEdgePropertyMap extends ModelPropertyMap {

    public ModelEdgePropertyMap(HashMap<String, String> map) {
        super(map);
    }

    public ModelEdgePropertyMap(final String label, String gid, String name, String processModelInstanceType, String edgeType) {
        super(label, gid, name, processModelInstanceType);
        this.put("edgeType", edgeType);
    }
}
