package org.etothepii.model;

public enum GraphDBName {
    Tinkergraph,
    ArcadeDB,
    JanusGraph,
    Neo4j
}