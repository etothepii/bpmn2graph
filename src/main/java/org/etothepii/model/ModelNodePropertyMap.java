package org.etothepii.model;

import java.util.HashMap;

/**
 * A PropertyMap for Nodes, according to the graph database schema of our thesis
 */
public class ModelNodePropertyMap extends ModelPropertyMap {

    public ModelNodePropertyMap(HashMap<String, String> map) {
        super(map);
    }

    public ModelNodePropertyMap(final String label, String gid, String name, String processModelInstanceType, String nodeType,
                                String startTimeISO, String endTimeISO, String laneName, String metaNodeGid) {
        super(label, gid, name, processModelInstanceType);

        this.put("nodeType", nodeType);
        this.put("laneName", laneName);
        this.put("metaNodeGid", metaNodeGid);

        //properties that pretty much only make sense for instances
        this.put("startTimeISO", startTimeISO);
        this.put("endTimeISO", endTimeISO);
        this.put("randomSampleSensorData", "" + (Math.round(Math.random() * 100.0) / 100.0));
    }
}
