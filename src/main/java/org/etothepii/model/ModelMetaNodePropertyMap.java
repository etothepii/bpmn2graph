package org.etothepii.model;

import java.util.HashMap;

/**
 * A PropertyMap for MetaNodes, according to the graph database schema of our thesis
 */
public class ModelMetaNodePropertyMap extends ModelPropertyMap {

    public ModelMetaNodePropertyMap(HashMap<String, String> map) {
        super(map);
    }

    public ModelMetaNodePropertyMap(final String label, String gid, String name, String processModelInstanceType,
                                    String source, String sourceVersion, String description, String processVersion,
                                    String bpmnFilename) {
        super(label, gid, name, processModelInstanceType);

        this.put("nodeType", "Meta");

        //properties that pretty much only make sense for meta nodes of models
        this.put("metaNodeGid", gid);
        this.put("source", source);
        this.put("sourceVersion", sourceVersion);
        this.put("description", description);
        this.put("processVersion", processVersion);
        this.put("bpmnFilename", bpmnFilename);
    }
}
