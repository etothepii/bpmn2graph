package org.etothepii.model.query;

import org.apache.commons.lang.NotImplementedException;
import org.etothepii.model.ModelPropertyMap;
import org.javatuples.Pair;

/**
 * A creator for Gremlin String queries.
 */
public class GremlinStringQueryCreator implements QueryCreator<String> {
    @Override
    public String clearNodesAndEdges() {
        throw new NotImplementedException();
    }

    @Override
    public String createNode(ModelPropertyMap properties) {
        throw new NotImplementedException();
    }

    @Override
    public String matchNodesByProperty(String propertyName, String propertyValue) {
        throw new NotImplementedException();
    }

    @Override
    public String matchEdgesByProperty(String propertyName, String propertyValue) {
        throw new NotImplementedException();
    }

    @Override
    public String createEdgeBetweenNodes(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        throw new NotImplementedException();
    }

    @Override
    public String rq1() {
        throw new NotImplementedException();
    }

    @Override
    public String rq2() {
        throw new NotImplementedException();
    }

    @Override
    public String rq3() {
        throw new NotImplementedException();
    }

    @Override
    public String rq4() {
        return "g.V().filter({it.get().property(\"name\").toString().toLowerCase().contains(\"shipment\")});";
    }

    @Override
    public String rq5() {
        throw new NotImplementedException();
    }

    @Override
    public String rq6() {
        throw new NotImplementedException();
    }

    @Override
    public String rq7() {
        throw new NotImplementedException();
    }

    @Override
    public String rq8() {
        throw new NotImplementedException();
    }
}
