package org.etothepii.model.query;

import org.etothepii.model.ModelPropertyMap;
import org.javatuples.Pair;

/**
 * A Creator for the special SQL dialect queries of OrientDB
 */
public class OrientDBQueryCreator implements QueryCreator<String[]> {

    @Override
    public String[] clearNodesAndEdges() {
        return new String[0];
    }

    @Override
    public String[] createNode(ModelPropertyMap properties) {
        return new String[]{
                "CREATE VERTEX MyNode CONTENT " + properties.toJson(true)
        };
    }

    @Override
    public String[] matchNodesByProperty(String propertyName, String propertyValue) {
        return new String[0];
    }

    @Override
    public String[] matchEdgesByProperty(String propertyName, String propertyValue) {
        return new String[0];
    }

    @Override
    public String[] createEdgeBetweenNodes(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        String findFromQuery = "SELECT FROM MyNode WHERE " + idPropertyOfVertexFrom.getValue0() + " = \"" + idPropertyOfVertexFrom.getValue1() + "\"";
        String findToQuery = "SELECT FROM MyNode WHERE " + idPropertyOfVertexTo.getValue0() + " = \"" + idPropertyOfVertexTo.getValue1() + "\"";
        return new String[]{
                "CREATE Edge MyEdge FROM (" + findFromQuery + ") TO (" + findToQuery + ") CONTENT " + properties.toJson(true)
        };
    }

    @Override
    public String[] rq1() {
        return new String[0];
    }

    @Override
    public String[] rq2() {
        return new String[0];
    }

    @Override
    public String[] rq3() {
        return new String[0];
    }

    @Override
    public String[] rq4() {
        return new String[0];
    }

    @Override
    public String[] rq5() {
        return new String[0];
    }

    @Override
    public String[] rq6() {
        return new String[0];
    }

    @Override
    public String[] rq7() {
        return new String[0];
    }

    @Override
    public String[] rq8() {
        return new String[0];
    }
}
