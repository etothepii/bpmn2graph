package org.etothepii.model.query;

import org.apache.commons.lang.NotImplementedException;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.etothepii.model.ModelPropertyMap;
import org.etothepii.model.tinkerpop.SimpleGraphDBConnector;
import org.javatuples.Pair;

/**
 * A Creator for Gremlin Queries.
 * The creator is almost never used in the thesis evaluation, since gremlin strings are sufficient for the tested databases. See GremlinStringQueryCreator.
 */
public class GremlinQueryCreator implements QueryCreator<GraphTraversal> {

    private SimpleGraphDBConnector connector;

    public GremlinQueryCreator(SimpleGraphDBConnector connector) {
        this.connector = connector;
    }

    @Override
    public GraphTraversal clearNodesAndEdges() {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal createNode(ModelPropertyMap properties) {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal matchNodesByProperty(String propertyName, String propertyValue) {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal matchEdgesByProperty(String propertyName, String propertyValue) {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal createEdgeBetweenNodes(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal rq1() {
        return this.connector.g.V().hasLabel("ParallelGateway");
    }

    @Override
    public GraphTraversal rq2() {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal rq3() {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal rq4() {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal rq5() {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal rq6() {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal rq7() {
        throw new NotImplementedException();
    }

    @Override
    public GraphTraversal rq8() {
        throw new NotImplementedException();
    }
}
