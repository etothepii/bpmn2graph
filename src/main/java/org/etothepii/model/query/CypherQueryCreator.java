package org.etothepii.model.query;

import org.etothepii.model.ModelNodePropertyMap;
import org.etothepii.model.ModelPropertyMap;
import org.javatuples.Pair;

/**
 * A Creator for Cypher Queries.
 */
public class CypherQueryCreator implements QueryCreator<String> {

    public CypherQueryCreator() {
        super();
    }

    public String clearNodesAndEdges() {
        return "MATCH (n) DETACH DELETE n;";
    }

    public String createNode(ModelPropertyMap properties) {
        return "CREATE (x:" + properties.label + " " + properties.toJson() + ");";
    }

    public String matchNodesByProperty(String propertyName, String propertyValue) {
        return "MATCH (x {" + propertyName + ":\"" + propertyValue + "\"}) RETURN x;";
    }

    public String matchEdgesByProperty(String propertyName, String propertyValue) {
        return "MATCH ~[y {" + propertyName + ":\"" + propertyValue + "\"}]~;";
    }

    public String createEdgeBetweenNodes(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        return "MATCH (x {" + idPropertyOfVertexFrom.getValue0() + ":\"" + idPropertyOfVertexFrom.getValue1() + "\"})" +
                ", (y {" + idPropertyOfVertexTo.getValue0() + ":\"" + idPropertyOfVertexTo.getValue1() + "\"}) " +
                "CREATE (x)-[z:" + properties.label + " " + properties.toJson() + "]->(y);";
    }


    public String rq1() {
        return "match (x :ParallelGateway) return count(*);";
    }
    public String rq2() {
        return "match (x {nodeType: \"Gateway\"})-[]->(y {nodeType: \"Gateway\"}) return count(*);";
    }
    public String rq3() {
        return "match (a :StartEvent)-[]->(b {nodeType: \"Task\"})-[]->(c {nodeType: \"Task\"})-[]->(d {nodeType: \"Gateway\"})-[]-(e {nodeType: \"Task\"})-[]-(f {nodeType: \"Gateway\"}) return count(*);";
    }
    public String rq4() {
        return "match (b {nodeType: \"Task\"}) where toLower(b.name) contains \"data\" return count(*);";
    }
    public String rq5() {
        return "match (k) with size((k)-[]->()) as outdegree where outdegree > 5 return count(*);";
    }
    public String rq6() {
        return "match (x :StartEvent)-[*..7]->(y {nodeType: \"Task\"}) return count(*);";
    }

    public String rq7() {
        return "match (n {nodeType: \"Meta\", source: \"Camunda Modeler\", processModelInstanceType: \"Instance\"}) return n.gid;";
    }

    public String rq8() {
        return "match (n {nodeType: \"Meta\", processModelInstanceType: \"Instance\"})-[:startsAt]->(x :StartEvent {name: \"\"}) return n.gid;";
    }
}
