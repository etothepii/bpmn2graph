package org.etothepii.model.query;

import org.etothepii.model.ModelPropertyMap;
import org.javatuples.Pair;

/**
 * The query creator is a class responsible for creating queries in the indicated language.
 * It contains methods to clear the DB, create a node, create an edge, and some more.
 * @param <T> the type of queries a creator makes. Some make strings, some make graphtraversals, etc...
 */
public interface QueryCreator<T> {
    public abstract T clearNodesAndEdges();
    public abstract T createNode(ModelPropertyMap properties);
    public abstract T matchNodesByProperty(String propertyName, String propertyValue);
    public abstract T matchEdgesByProperty(String propertyName, String propertyValue);
    public abstract T createEdgeBetweenNodes(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties);

    public abstract T rq1();
    public abstract T rq2();
    public abstract T rq3();
    public abstract T rq4();
    public abstract T rq5();
    public abstract T rq6();
    public abstract T rq7();
    public abstract T rq8();
}
