package org.etothepii.model;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * An extension of the Hashmap. It can store properties (used in property graphs). The ModelPropertyMap ensures that there are certain properties always existing in the map.
 */
public abstract class ModelPropertyMap extends HashMap<String, String> {

    public final String label;

    public ModelPropertyMap(Map<String, String> map) {
        this.label = map.get("label");
        map.entrySet().stream().filter(e -> !e.getKey().equalsIgnoreCase("label")).forEach(e -> this.put(e.getKey(), e.getValue()));
    }

    public ModelPropertyMap(final String label, String gid, String name, String processModelInstanceType) {
        this.label = label;
        this.put("gid", gid);
        this.put("name", name);
        this.put("processModelInstanceType", processModelInstanceType);
    }

    /**
     * This method returns a JSON string consisting of the properties
     * @return the JSON string containing the proeprties
     */
    public String toJson() {
        return this.toJson(false);
    }
    public String toJson(boolean includeLabel) {
        return "{" + this.entrySet().stream().map(e -> e.getKey() + ":" + "\"" + e.getValue() + "\"").collect(Collectors.joining(", ")) +
                ((includeLabel) ? ", label: \"" + this.label + "\"" : "") + "}";
    }

    /**
     * Ensures that we do not put null into the property map.
     * @param name key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the value
     */
    @Override
    public String put(String name, String value) {
        return super.put(
                name != null ? name.replace("\"", "") : "null",
                value != null ? value.replace("\"", "") : "null"
        );
    }
}

