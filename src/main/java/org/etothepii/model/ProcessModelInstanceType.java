package org.etothepii.model;

/**
 * An enum specifying whether something is a Model, Instance or MetaNode
 */
public enum ProcessModelInstanceType {
    MODEL("Model"),
    INSTANCE("Instance"),
    METANODE("MetaNode");

    private final String text;

    ProcessModelInstanceType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
