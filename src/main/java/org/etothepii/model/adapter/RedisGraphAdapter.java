package org.etothepii.model.adapter;

import com.redislabs.redisgraph.ResultSet;
import com.redislabs.redisgraph.impl.api.RedisGraph;
import org.apache.commons.lang.NotImplementedException;
import org.etothepii.ThesisEvaluationFetchQueries;
import org.etothepii.model.ModelPropertyMap;
import org.etothepii.model.query.CypherQueryCreator;
import org.javatuples.Pair;

import java.util.Collection;
import java.util.LinkedList;

public class RedisGraphAdapter extends GraphDBJavaAdapter implements ThesisEvaluationFetchQueries {

    private final RedisGraph graph;
    private CypherQueryCreator queryCreator;

    public RedisGraphAdapter() {
        this.queryCreator = new CypherQueryCreator();
        this.graph = new RedisGraph("localhost", 6379);
    }

    private void tryQuery(String query) {
        try {
            //RedisGraph does not support more than one statement at once.
            //So we replace the semicolons, otherwise errors get thrown -.-
            this.graph.query("mygraph", query.replace(";", ""));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private int tryQueryInt(String query) {
        int answer = -1;
        try {
            ResultSet rs = this.graph.query("mygraph", query.replace(";", ""));
            answer = Math.toIntExact(rs.next().getValue(0));
        } catch(Exception e) {
            e.printStackTrace();
        }
        return answer;
    }

    private Object tryQueryObject(String query) {
        Object answer = null;
        try {
            ResultSet rs = this.graph.query("mygraph", query.replace(";", ""));
            answer = rs.next().getValue(0);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return answer;
    }

    private Collection<String> tryQueryStrings(String query) {
        Collection<String> answer = new LinkedList<>();
        try {
            ResultSet rs = this.graph.query("mygraph", query.replace(";", ""));
            while (rs.hasNext()) {
                answer.add(rs.next().getString(0));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return answer;
    }

    @Override
    public void clearDB() {
        this.tryQuery(this.queryCreator.clearNodesAndEdges());
    }

    @Override
    public void addVertex(ModelPropertyMap properties) {
        this.tryQuery(this.queryCreator.createNode(properties));
    }

    @Override
    public Object getVertexByProperty(String propertyName, String propertyValue) {
        return this.tryQueryObject(this.queryCreator.matchNodesByProperty(propertyName, propertyValue));
    }

    @Override
    public void addEdge(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        this.tryQuery(this.queryCreator.createEdgeBetweenNodes(idPropertyOfVertexFrom, idPropertyOfVertexTo, properties));
    }

    @Override
    public Object getEdgeByProperty(String propertyName, String propertyValue) {
        return this.tryQueryObject(this.queryCreator.matchEdgesByProperty(propertyName, propertyValue));
    }

    @Override
    public void close() {
        try {
            this.graph.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCountOfParallelGateways() {
        return this.tryQueryInt(this.queryCreator.rq1());
    }

    @Override
    public int getCountOfPattern2SuccessiveTasks() {
        return this.tryQueryInt(this.queryCreator.rq2());
    }

    @Override
    public int getCountOfPatternStarteventTaskTaskGatewayTaskGateway() {
        return this.tryQueryInt(this.queryCreator.rq3());
    }

    @Override
    public int getCountOfTasksWithDataInName() {
        return this.tryQueryInt(this.queryCreator.rq4());
    }

    @Override
    public int getCountOfNodesWithMoreThanFiveOutgoingEdges() {
        return this.tryQueryInt(this.queryCreator.rq5());
    }

    @Override
    public int getCountOfStartEventsConnectedToATaskOver7StepsMaximum() {
        return this.tryQueryInt(this.queryCreator.rq6());
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesFromCamundaModeler() {
        return this.tryQueryStrings(this.queryCreator.rq7());
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesWhereStartEventNameEmpty() {
        return this.tryQueryStrings(this.queryCreator.rq7());
    }
}
