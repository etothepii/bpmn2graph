package org.etothepii.model.adapter;

import com.orientechnologies.orient.jdbc.OrientJdbcConnection;
import com.orientechnologies.orient.jdbc.OrientJdbcDriver;
import org.apache.commons.lang.NotImplementedException;
import org.etothepii.ThesisEvaluationFetchQueries;
import org.etothepii.model.ModelNodePropertyMap;
import org.etothepii.model.ModelPropertyMap;
import org.etothepii.model.query.OrientDBQueryCreator;
import org.javatuples.Pair;
import org.postgresql.util.PSQLException;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Properties;
import java.util.stream.Collectors;

public class OrientDBAdapter extends GraphDBJavaAdapter implements ThesisEvaluationFetchQueries {

    private Connection conn;
    private OrientDBQueryCreator queryCreator;

    public OrientDBAdapter() throws SQLException {

        this.queryCreator = new OrientDBQueryCreator();
        Properties info = new Properties();
        info.put("user", "root");
        info.put("password", "orientdb");
        this.conn = (OrientJdbcConnection) DriverManager.getConnection("jdbc:orient:remote:localhost/bpmn2graph2", info);
    }

    @Override
    public void clearDB() {
        try {
            Statement stmt = conn.createStatement();
            stmt.executeQuery("DROP CLASS MyNode IF EXISTS UNSAFE;");
            stmt.executeQuery("DROP CLASS MyEdge IF EXISTS UNSAFE;");
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            Statement stmt = conn.createStatement();
            stmt.executeQuery("CREATE CLASS MyNode extends V");
            stmt.executeQuery("CREATE CLASS MyEdge extends E");
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addVertex(ModelPropertyMap properties) {
        try {
            Statement stmt = conn.createStatement();

            if (properties.containsKey("name") && properties.get("name").contains("\n")) {
                properties.put("name", properties.get("name").replace("\n", ""));
            }

            String query = this.queryCreator.createNode(properties)[0];
            stmt.executeQuery(query);
            stmt.close();
        } catch(SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    @Override
    public Object getVertexByProperty(String propertyName, String propertyValue) {
        return this.tryQueryObject(
                "select MyNode\n" +
                "\tfrom MyNode\n" +
                "\twhere " + propertyName + " = \"" + propertyValue + "\"\n"
        );
    }

    @Override
    public void addEdge(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        try {
            Statement stmt = conn.createStatement();
            String query = this.queryCreator.createEdgeBetweenNodes(idPropertyOfVertexFrom, idPropertyOfVertexTo, properties)[0];
            stmt.executeQuery(query);
            stmt.close();
        } catch(SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    @Override
    public Object getEdgeByProperty(String propertyName, String propertyValue) {
        return this.tryQueryObject(
                "select MyEdge\n" +
                "\tfrom MyEdge\n" +
                "\twhere " + propertyName + " = \"" + propertyValue + "\"\n"
        );
    }

    @Override
    public void close() {
        try {
            this.conn.close();
        }catch (Exception ignore) {}
    }

    private int tryQueryInt(String query) {
        int answer = -1;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            if (rs.next()) {
                answer = Math.toIntExact(rs.getLong(1));
            }

            rs.close();
            stmt.close();
        } catch(SQLException sqle) {
            sqle.printStackTrace();
        }

        return answer;
    }

    private Object tryQueryObject(String query) {
        Object answer = null;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            if (rs.next()) {
                answer = rs.getObject(1);
            }

            rs.close();
            stmt.close();
        } catch(SQLException sqle) {
            sqle.printStackTrace();
        }

        return answer;
    }

    private Collection<String> tryStringsFetchQuery(String query) {
        Collection<String> answer = new LinkedList<>();
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(query.replace("\"", "'"));

            while (rs.next()) {
                answer.add(rs.getString(1));
            }

            rs.close();
            stmt.close();
        } catch(PSQLException ignore) {
            //
        } catch(Exception e) {
            e.printStackTrace();
        }

        return answer;
    }

    @Override
    public int getCountOfParallelGateways() {
        return this.tryQueryInt("SELECT count(*) FROM MyNode WHERE label = \"ParallelGateway\"");
    }

    @Override
    public int getCountOfPattern2SuccessiveTasks() {
        //sql queries are already gettin' big...
        return this.tryQueryInt(
                "select count(*) from (\n" +
                "\tselect expand(out()) as x\n" +
                "\tfrom MyNode\n" +
                "\twhere nodeType = \"Task\"\n" +
                ")\n" +
                "where nodeType = \"Task\""
        );
    }

    @Override
    public int getCountOfPatternStarteventTaskTaskGatewayTaskGateway() {
        //not the most beautiful solution. But the best we could come up with.

        return this.tryQueryInt(
                "select count(*) from (\n" +
                "\tselect expand(out()) from (\n" +
                "        select expand(out()) from (\n" +
                "            select expand(out()) from (\n" +
                "                select expand(out()) from (\n" +
                "                    select expand(out())\n" +
                "                    from MyNode\n" +
                "                    where label = \"StartEvent\"\n" +
                "                )\n" +
                "                where nodeType = \"Task\"\n" +
                "            )\n" +
                "            where nodeType = \"Task\"\n" +
                "        )\n" +
                "        where nodeType = \"Gateway\"\n" +
                "    )\n" +
                "    where nodeType = \"Task\"\n" +
                ")\n" +
                "where nodeType = \"Gateway\""
        );

    }

    @Override
    public int getCountOfTasksWithDataInName() {
        return this.tryQueryInt(
                "select count(*)\n" +
                "from MyNode\n" +
                "where nodeType = \"Task\" and name.toLowerCase() containsText \"data\""
        );
    }

    @Override
    public int getCountOfNodesWithMoreThanFiveOutgoingEdges() {
        return this.tryQueryInt(
                "select count(*)\n" +
                "from MyNode\n" +
                "where out().size() > 5"
        );
    }

    @Override
    public int getCountOfStartEventsConnectedToATaskOver7StepsMaximum() {
        return this.tryQueryInt(
                "select count(*) from (\n" +
                "    TRAVERSE out() FROM (\n" +
                "        select * from MyNode where label = \"StartEvent\"\n" +
                "    ) MAXDEPTH 7\n" +
                ")\n" +
                "where nodeType = \"Task\""
        );
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesFromCamundaModeler() {
        return this.tryStringsFetchQuery(
                "select gid from MyNode\n" +
                "where nodeType = \"Meta\" and source = \"Camunda Modeler\" and processModelInstanceType = \"Instance\""
        );
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesWhereStartEventNameEmpty() {
        return this.tryStringsFetchQuery(
                "select gid from (\n" +
                "  \tselect * from (\n" +
                "    \tselect expand(out())\n" +
                "    \tfrom MyNode\n" +
                "    \twhere nodeType = \"Meta\" and processModelInstanceType = \"Instance\"\n" +
                "  \t)\n" +
                "  \twhere label = \"StartEvent\"\n" +
                ")"
        );
    }
}
