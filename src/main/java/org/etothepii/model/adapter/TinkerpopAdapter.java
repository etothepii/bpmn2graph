package org.etothepii.model.adapter;

import org.apache.commons.lang.NotImplementedException;
import org.apache.tinkerpop.gremlin.process.traversal.Traversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.etothepii.ThesisEvaluationFetchQueries;
import org.etothepii.model.GraphDBName;
import org.etothepii.model.ModelPropertyMap;
import org.etothepii.model.query.GremlinQueryCreator;
import org.etothepii.model.tinkerpop.DBConnectionType;
import org.etothepii.model.tinkerpop.SimpleGraphDBConnector;
import org.javatuples.Pair;
import org.neo4j.driver.Result;

import javax.naming.OperationNotSupportedException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TinkerpopAdapter extends GraphDBJavaAdapter implements ThesisEvaluationFetchQueries {

    public final SimpleGraphDBConnector connector;
    private final GremlinQueryCreator queryCreator;

    public TinkerpopAdapter() throws OperationNotSupportedException {
        this(GraphDBName.Tinkergraph, DBConnectionType.Inmemory);
    }
    public TinkerpopAdapter(GraphDBName dbName, DBConnectionType connectionType) throws OperationNotSupportedException {
        super();
        this.connector = new SimpleGraphDBConnector(dbName, connectionType);
        this.queryCreator = new GremlinQueryCreator(this.connector);
    }

    @Override
    public void clearDB() {
        this.connector.g.V().drop().iterate();
    }

    @Override
    public void close() {
        this.connector.close();
    }

    @Override
    public void addVertex(ModelPropertyMap properties) {
        GraphTraversal<Vertex, Vertex> t = this.connector.g.addV(properties.label);
        properties.forEach(t::property);
        t.iterate();
    }

    @Override
    public Object getVertexByProperty(String propertyName, String propertyValue) {
        return this.connector.g.V().has(propertyName, propertyValue).limit(1).next();
    }

    @Override
    public void addEdge(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        Vertex from = this.connector.g.V().has(idPropertyOfVertexFrom.getValue0(), idPropertyOfVertexFrom.getValue1()).next();
        Vertex to = this.connector.g.V().has(idPropertyOfVertexTo.getValue0(), idPropertyOfVertexTo.getValue1()).next();

        if (from != null && to != null){
            GraphTraversal<Edge, Edge> t = this.connector.g.addE(properties.label).from(from).to(to);
            properties.forEach(t::property);
            t.iterate();
        }
    }

    @Override
    public Object getEdgeByProperty(String propertyName, String propertyValue) {
        return this.connector.g.E().has(propertyName, propertyValue).limit(1).next();
    }

    @Override
    public int getCountOfParallelGateways() {
        return Math.toIntExact((Long) this.queryCreator.rq1().count().next());
    }

    @Override
    public int getCountOfPattern2SuccessiveTasks() {
        return Math.toIntExact(this.connector.g.V().has("nodeType", "Task").out().has("nodeType", "Task").count().next());
    }

    @Override
    public int getCountOfPatternStarteventTaskTaskGatewayTaskGateway() {
        return Math.toIntExact(
                this.connector.g.V().hasLabel("StartEvent")
                        .out().has("nodeType", "Task")
                        .out().has("nodeType", "Task")
                        .out().has("nodeType", "Gateway")
                        .out().has("nodeType", "Task")
                        .out().has("nodeType", "Gateway")
                        .count().next()
        );
    }

    @Override
    public int getCountOfTasksWithDataInName() {
        return Math.toIntExact(
                this.connector.g.V().has("nodeType", "Task").filter(it ->
                        it.get().property("name").toString().toLowerCase().contains("data")
                ).count().next()
        );
    }

    @Override
    public int getCountOfNodesWithMoreThanFiveOutgoingEdges() {
        return Math.toIntExact(
                this.connector.g.V().filter(it ->
                        this.connector.g.V(it.get()).outE().count().next() > 5
                ).count().next()
        );
    }

    @Override
    public int getCountOfStartEventsConnectedToATaskOver7StepsMaximum() {

        //TODO: If someone has a better idea let me know.
        //this certainly isnt the most efficient query.
        //but gremlin is kinda complicated in some ways.

        int int1 = Math.toIntExact(
                this.connector.g.V().hasLabel("StartEvent")
                        .repeat(__.out()).times(1)
                        .hasLabel("Task")
                        .count().next());

        int int2 = Math.toIntExact(
                this.connector.g.V().hasLabel("StartEvent")
                        .repeat(__.out()).times(2)
                        .hasLabel("Task")
                        .count().next());

        int int3 = Math.toIntExact(
                this.connector.g.V().hasLabel("StartEvent")
                        .repeat(__.out()).times(3)
                        .hasLabel("Task")
                        .count().next());

        int int4 = Math.toIntExact(
                this.connector.g.V().hasLabel("StartEvent")
                        .repeat(__.out()).times(4)
                        .hasLabel("Task")
                        .count().next());

        int int5 = Math.toIntExact(
                this.connector.g.V().hasLabel("StartEvent")
                        .repeat(__.out()).times(5)
                        .hasLabel("Task")
                        .count().next());

        int int6 = Math.toIntExact(
                this.connector.g.V().hasLabel("StartEvent")
                        .repeat(__.out()).times(6)
                        .hasLabel("Task")
                        .count().next());

        int int7 = Math.toIntExact(
                this.connector.g.V().hasLabel("StartEvent")
                        .repeat(__.out()).times(7)
                        .hasLabel("Task")
                        .count().next());

        return int1 + int2 + int3 + int4 + int5 + int6 + int7;
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesFromCamundaModeler() {
        return this.connector.g.V().has("nodeType", "Meta")
                .has("source", "Camunda Modeler")
                .has("processModelInstanceType", "Instance")
                .select("gid").toStream().map(o -> o.toString()).collect(Collectors.toList());
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesWhereStartEventNameEmpty() {
        return this.connector.g.V().has("nodeType", "Meta")
                .has("processModelInstanceType", "Instance")
                .outE("startsAt").inV().has("name", "")
                .select("gid").toStream().map(o -> o.toString()).collect(Collectors.toList());
    }
}
