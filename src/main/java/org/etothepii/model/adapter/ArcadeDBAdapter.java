package org.etothepii.model.adapter;

import org.apache.commons.lang.NotImplementedException;
import org.etothepii.ThesisEvaluationFetchQueries;
import org.etothepii.model.ModelPropertyMap;
import org.etothepii.model.query.CypherQueryCreator;
import org.etothepii.model.query.GremlinStringQueryCreator;
import org.javatuples.Pair;
import org.postgresql.util.PSQLException;

import java.sql.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Properties;

/**
 * An adapter for the ArcadeDB graph database.
 */
public class ArcadeDBAdapter extends GraphDBJavaAdapter implements ThesisEvaluationFetchQueries {

    private Connection conn;
    private CypherQueryCreator queryCreator;
    private GremlinStringQueryCreator gremlinStringQueryCreator;


    public ArcadeDBAdapter() {

        this.queryCreator = new CypherQueryCreator();
        this.gremlinStringQueryCreator = new GremlinStringQueryCreator();

        try {

            Class.forName("org.postgresql.Driver");

            Properties props = new Properties();
            props.setProperty("user", "root");
            props.setProperty("password", "arcadedb");
            props.setProperty("ssl", "false");
            this.conn = DriverManager.getConnection("jdbc:postgresql://localhost/mydb", props);

            //can be that under windows the postgresql plugin is not loaded. so you need the command below before starting.
            //set ARCADEDB_SETTINGS=-Darcadedb.server.plugins="Postgres:com.arcadedb.postgres.PostgresProtocolPlugin"

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void tryQuery(String query) {

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int tryQueryInt(String query) {
        return this.tryQueryInt(query, false);
    }
    private int tryQueryInt(String query, boolean justCountTheResultsInstead) {
        int answer = -1;

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            if (rs.next()) {
                if (justCountTheResultsInstead) {
                    do {
                        answer = rs.getRow();
                    } while(rs.next());
                } else {
                    answer = rs.getInt(1);
                }
            }

            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return answer;
    }

    private Collection<String> tryStringsFetchQuery(String query) {
        Collection<String> answer = new LinkedList<>();
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(query.replace("\"", "'"));

            while (rs.next()) {
                answer.add(rs.getString(1));
            }

            rs.close();
            stmt.close();
        } catch(PSQLException ignore) {
            //
        } catch(Exception e) {
            e.printStackTrace();
        }

        return answer;
    }

    private Object tryObjectQuery(String query) {
        Object answer = null;
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(query.replace("\"", "'"));

            if (rs.next()) {
                answer = rs.getObject(1);
            }

            rs.close();
            stmt.close();
        } catch(PSQLException ignore) {
            //
        } catch(Exception e) {
            e.printStackTrace();
        }

        return answer;
    }


    @Override
    public void clearDB() {

        //arcadedb seems to have issues deleting matched elements, especially in cypher!? so we use gremlin. Cool that arcadeDB supports multiple languages.
        this.tryQuery("{gremlin} g.E().drop();");
        this.tryQuery("{gremlin} g.V().drop();");
    }

    @Override
    public void addVertex(ModelPropertyMap properties) {
        this.tryQuery("{cypher} " + this.queryCreator.createNode(properties));
    }

    @Override
    public Object getVertexByProperty(String propertyName, String propertyValue) {
        return this.tryObjectQuery("{cypher} " + this.queryCreator.matchNodesByProperty(propertyName, propertyValue));
    }

    @Override
    public void addEdge(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        this.tryQuery("{cypher} " + this.queryCreator.createEdgeBetweenNodes(idPropertyOfVertexFrom, idPropertyOfVertexTo, properties));
    }

    @Override
    public Object getEdgeByProperty(String propertyName, String propertyValue) {
        return this.tryObjectQuery("{cypher} " + this.queryCreator.matchEdgesByProperty(propertyName, propertyValue));
    }

    @Override
    public void close() {
        try {
            this.conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCountOfParallelGateways() {
        return this.tryQueryInt("{cypher} " + this.queryCreator.rq1());
    }

    @Override
    public int getCountOfPattern2SuccessiveTasks() {
        return this.tryQueryInt("{cypher} " + this.queryCreator.rq2());
    }

    @Override
    public int getCountOfPatternStarteventTaskTaskGatewayTaskGateway() {
        return this.tryQueryInt("{cypher} " + this.queryCreator.rq3());
    }

    @Override
    public int getCountOfTasksWithDataInName() {
        //Arcadedb doesnt support some more custom cypher functions out of the box. such as toLower.
        //Easiest is, we use the gremlin language.

        //Second issue is, that it seems no ints or longs are supported as answer. Means we cant count the results in the gremlin query.
        //But have to return a traverser in the gremlin query ugh.
        //I mean, this really complicates things once you wanna do something efficient with it.

        return this.tryQueryInt("{gremlin} " + this.gremlinStringQueryCreator.rq4(), true);
    }

    @Override
    public int getCountOfNodesWithMoreThanFiveOutgoingEdges() {
        return this.tryQueryInt("{cypher} " + this.queryCreator.rq5());
    }

    @Override
    public int getCountOfStartEventsConnectedToATaskOver7StepsMaximum() {
        return this.tryQueryInt("{cypher} " + this.queryCreator.rq6());
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesFromCamundaModeler() {
        return this.tryStringsFetchQuery("{cypher} " + this.queryCreator.rq7());
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesWhereStartEventNameEmpty() {
        return this.tryStringsFetchQuery("{cypher} " + this.queryCreator.rq8());
    }
}
