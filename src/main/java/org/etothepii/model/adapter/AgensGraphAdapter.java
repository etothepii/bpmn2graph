package org.etothepii.model.adapter;

import org.apache.commons.lang.NotImplementedException;
import org.etothepii.ThesisEvaluationFetchQueries;
import org.etothepii.model.ModelPropertyMap;
import org.etothepii.model.query.CypherQueryCreator;
import org.javatuples.Pair;
import org.postgresql.util.PSQLException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.LinkedList;



/**
 * An adapter for the ArcadeDB graph database.
 */
public class AgensGraphAdapter extends GraphDBJavaAdapter implements ThesisEvaluationFetchQueries {

    private Connection conn = null;
    private CypherQueryCreator cypherQueryCreator;

    public AgensGraphAdapter() {
        this.cypherQueryCreator = new CypherQueryCreator();

        try {
            //docker run --name agensgraph -e POSTGRES_PASSWORD=agensgraph -d bitnine/agensgraph:latest
            //ALTER USER postgres SET graph_path = 'agens';
            Class.forName("net.bitnine.agensgraph.Driver");
            String connectionString = "jdbc:agensgraph://127.0.0.1:5432/";
            String username = "postgres";
            String password = "agensgraph";
            this.conn = DriverManager.getConnection(connectionString, username, password);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void tryQuery(String query) {
        try {
            //AgensGraph query have their own charme. Not recognizing double ticks ("), but rather only single ticks (').
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(query.replace("\"", "'"));

            rs.close();
            stmt.close();
        } catch(PSQLException ignore) {
            //also, agensgraph throws PSQL errors when no result is returned. Cool, right?
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private Object tryObjectQuery(String query) {
        Object answer = null;
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(query.replace("\"", "'"));

            if (rs.next()) {
                answer = rs.getObject(1);
            }

            rs.close();
            stmt.close();
        } catch(PSQLException ignore) {
            //
        } catch(Exception e) {
            e.printStackTrace();
        }

        return answer;
    }

    @Override
    public void clearDB() {
        this.tryQuery(this.cypherQueryCreator.clearNodesAndEdges());
    }

    @Override
    public void addVertex(ModelPropertyMap properties) {
        this.tryQuery(this.cypherQueryCreator.createNode(properties));
    }

    @Override
    public Object getVertexByProperty(String propertyName, String propertyValue) {
        return this.tryObjectQuery(this.cypherQueryCreator.matchNodesByProperty(propertyName, propertyValue));
    }

    @Override
    public void addEdge(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        this.tryQuery(this.cypherQueryCreator.createEdgeBetweenNodes(idPropertyOfVertexFrom, idPropertyOfVertexTo, properties));
    }

    @Override
    public Object getEdgeByProperty(String propertyName, String propertyValue) {
        return this.tryObjectQuery(this.cypherQueryCreator.matchEdgesByProperty(propertyName, propertyValue));
    }

    @Override
    public void close() {
        try {
            this.conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private int tryIntFetchQuery(String query) {
        int answer = -1;
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(query.replace("\"", "'"));

            if (rs.next()) {
                //the index of columns begins at 1.
                answer = rs.getInt(1);
            }

            rs.close();
            stmt.close();
        } catch(PSQLException ignore) {
            //
        } catch(Exception e) {
            e.printStackTrace();
        }

        return answer;
    }

    private Collection<String> tryStringsFetchQuery(String query) {
        Collection<String> answer = new LinkedList<>();
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(query.replace("\"", "'"));

            while (rs.next()) {
                answer.add(rs.getString(1));
            }

            rs.close();
            stmt.close();
        } catch(PSQLException ignore) {
            //
        } catch(Exception e) {
            e.printStackTrace();
        }

        return answer;
    }

    @Override
    public int getCountOfParallelGateways() {
        return this.tryIntFetchQuery(this.cypherQueryCreator.rq1());
    }

    @Override
    public int getCountOfPattern2SuccessiveTasks() {
        return this.tryIntFetchQuery(this.cypherQueryCreator.rq2());
    }

    @Override
    public int getCountOfPatternStarteventTaskTaskGatewayTaskGateway() {
        return this.tryIntFetchQuery(this.cypherQueryCreator.rq3());
    }

    @Override
    public int getCountOfTasksWithDataInName() {
        return this.tryIntFetchQuery(this.cypherQueryCreator.rq4());
    }

    @Override
    public int getCountOfNodesWithMoreThanFiveOutgoingEdges() {
        return this.tryIntFetchQuery(this.cypherQueryCreator.rq5());
    }

    @Override
    public int getCountOfStartEventsConnectedToATaskOver7StepsMaximum() {
        return this.tryIntFetchQuery(this.cypherQueryCreator.rq6());
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesFromCamundaModeler() {
        return this.tryStringsFetchQuery(this.cypherQueryCreator.rq7());
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesWhereStartEventNameEmpty() {
        return this.tryStringsFetchQuery(this.cypherQueryCreator.rq8());
    }
}
