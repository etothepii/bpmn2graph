package org.etothepii.model.adapter;

import org.etothepii.ThesisEvaluationFetchQueries;
import org.etothepii.model.*;
import org.etothepii.model.query.CypherQueryCreator;
import org.javatuples.Pair;
import org.neo4j.driver.*;
import org.neo4j.driver.Record;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * An adapter for the Neo4j graph database.
 */
public class Neo4jAdapter extends GraphDBJavaAdapter implements ThesisEvaluationFetchQueries {

    private final Driver driver;
    private CypherQueryCreator cypherQueryCreator;

    //Initialize and connect to a local neo4j database on port 7687. If your usernae + password differs, please edit the connection string.
    public Neo4jAdapter() {
        this.cypherQueryCreator = new CypherQueryCreator();
        this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic( "neo4j", "123" ) );
    }

    @Override
    public void clearDB() {
        final String query = this.cypherQueryCreator.clearNodesAndEdges();
        try ( Session session = driver.session() )
        {
            session.run(query);
        }
    }

    @Override
    public void close() {
        this.driver.close();
    }

    @Override
    public void addVertex(ModelPropertyMap properties) {

        final String query = this.cypherQueryCreator.createNode(properties);

        try ( Session session = driver.session() )
        {
            session.run(query);
        }
    }

    @Override
    public Object getVertexByProperty(String propertyName, String propertyValue) {
        final String query = this.cypherQueryCreator.matchNodesByProperty(propertyName, propertyValue);
        Object answer = null;

        try ( Session session = driver.session() )
        {
            Result res = session.run(query);
            answer = res.next();
        }

        return answer;
    }

    @Override
    public void addEdge(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        final String query = this.cypherQueryCreator.createEdgeBetweenNodes(idPropertyOfVertexFrom, idPropertyOfVertexTo, properties);

        try ( Session session = driver.session() )
        {
            session.run(query);
        }
    }

    @Override
    public Object getEdgeByProperty(String propertyName, String propertyValue) {
        final String query = this.cypherQueryCreator.matchEdgesByProperty(propertyName, propertyValue);
        Object answer = null;

        try ( Session session = driver.session() )
        {
            Result res = session.run(query);
            answer = res.next();
        }

        return answer;
    }

    @Override
    public int getCountOfParallelGateways() {
        final String query = this.cypherQueryCreator.rq1();
        return tryQuery(query).get(0).asInt();
    }

    private Record tryQuery(String query) {
        try ( Session session = driver.session() )
        {
            return session.run(query).single();
        }
    }

    private Collection<String> tryQueryStrings(String query) {
        try ( Session session = driver.session() )
        {
            Result rs = session.run(query);
            Record rc = rs.next();
            return rs.list().stream().map(r -> r.get(0).asString()).collect(Collectors.toList());
        }
    }

    @Override
    public int getCountOfPattern2SuccessiveTasks() {
        final String query = this.cypherQueryCreator.rq2();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public int getCountOfPatternStarteventTaskTaskGatewayTaskGateway() {
        final String query = this.cypherQueryCreator.rq3();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public int getCountOfTasksWithDataInName() {
        final String query = this.cypherQueryCreator.rq4();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public int getCountOfNodesWithMoreThanFiveOutgoingEdges() {
        final String query = this.cypherQueryCreator.rq5();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public int getCountOfStartEventsConnectedToATaskOver7StepsMaximum() {
        final String query = this.cypherQueryCreator.rq6();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesFromCamundaModeler() {
        final String query = this.cypherQueryCreator.rq7();
        return tryQueryStrings(query);
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesWhereStartEventNameEmpty() {
        final String query = this.cypherQueryCreator.rq8();
        return tryQueryStrings(query);
    }
}
