package org.etothepii.model.adapter;

import org.etothepii.model.ModelPropertyMap;
import org.etothepii.model.query.CypherQueryCreator;
import org.etothepii.model.query.QueryCreator;
import org.javatuples.Pair;

/**
 * A typical class for adapters. Adapters are responsible for any queries towards a particular database.
 * Note that the readqueries RQ1-RQ8 for the evaulation in the thesis are defined in a different interface.
 */
public abstract class GraphDBJavaAdapter implements AutoCloseable {

    public abstract void clearDB();
    public abstract void addVertex(ModelPropertyMap properties);
    public abstract Object getVertexByProperty(String propertyName, String propertyValue);
    public abstract void addEdge(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties);
    public abstract Object getEdgeByProperty(String propertyName, String propertyValue);
    public abstract void close();
}
