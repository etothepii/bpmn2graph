package org.etothepii.model.adapter;

import org.apache.commons.lang.NotImplementedException;
import org.etothepii.ThesisEvaluationFetchQueries;
import org.etothepii.model.ModelPropertyMap;
import org.etothepii.model.query.CypherQueryCreator;
import org.javatuples.Pair;
import org.neo4j.driver.*;
import org.neo4j.driver.Record;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * An adapter for the Memgraph database.
 * Since much of its code is very similiar to the Neo4j Adapter, it may make sense in the future to combine those two under a common class.
 */
public class MemgraphAdapter extends GraphDBJavaAdapter implements ThesisEvaluationFetchQueries {

    private final Driver driver;
    private CypherQueryCreator cypherQueryCreator;
    public MemgraphAdapter() {
        super();

        this.cypherQueryCreator = new CypherQueryCreator();
        this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic( "", "" ) );
        try
        {
            this.driver.verifyConnectivity();
        } catch(Exception e) {
            e.printStackTrace();
        }

    }

    private void tryRunQuery(String query) {
        try ( Session session = this.driver.session() )
        {
            session.run(query);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void clearDB() {
        final String query = this.cypherQueryCreator.clearNodesAndEdges();
        try ( Session session = driver.session() )
        {
            session.run(query);
        }
    }

    @Override
    public void addVertex(ModelPropertyMap properties) {
        try ( Session session = driver.session() )
        {
            Result res = session.run(this.cypherQueryCreator.createNode(properties));
        }
    }

    @Override
    public Object getVertexByProperty(String propertyName, String propertyValue) {
        final String query = this.cypherQueryCreator.matchEdgesByProperty(propertyName, propertyValue);
        Object answer = null;

        try ( Session session = driver.session() )
        {
            Result res = session.run(query);
            answer = res.next();
        }

        return answer;
    }

    @Override
    public void addEdge(Pair<String, String> idPropertyOfVertexFrom, Pair<String, String> idPropertyOfVertexTo, ModelPropertyMap properties) {
        try ( Session session = driver.session() )
        {
            Result res = session.run(this.cypherQueryCreator.createEdgeBetweenNodes(idPropertyOfVertexFrom, idPropertyOfVertexTo, properties));
        }
    }

    @Override
    public Object getEdgeByProperty(String propertyName, String propertyValue) {
        final String query = this.cypherQueryCreator.matchEdgesByProperty(propertyName, propertyValue);
        Object answer = null;

        try ( Session session = driver.session() )
        {
            Result res = session.run(query);
            answer = res.next();
        }

        return answer;
    }

    private Record tryQuery(String query) {
        try ( Session session = driver.session() )
        {
            return session.run(query).single();
        }
    }

    private Collection<String> tryQueryStrings(String query) {
        try ( Session session = driver.session() )
        {
            Result rs = session.run(query);
            Record rc = rs.next();
            return rs.list().stream().map(r -> r.get(0).asString()).collect(Collectors.toList());
        }
    }

    @Override
    public void close() {
        this.driver.close();
    }


    @Override
    public int getCountOfParallelGateways() {
        final String query = this.cypherQueryCreator.rq1();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public int getCountOfPattern2SuccessiveTasks() {
        final String query = this.cypherQueryCreator.rq2();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public int getCountOfPatternStarteventTaskTaskGatewayTaskGateway() {
        final String query = this.cypherQueryCreator.rq3();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public int getCountOfTasksWithDataInName() {
        final String query = this.cypherQueryCreator.rq4();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public int getCountOfNodesWithMoreThanFiveOutgoingEdges() {
        final String query = this.cypherQueryCreator.rq5();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public int getCountOfStartEventsConnectedToATaskOver7StepsMaximum() {
        final String query = this.cypherQueryCreator.rq6();
        return tryQuery(query).get(0).asInt();
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesFromCamundaModeler() {
        final String query = this.cypherQueryCreator.rq7();
        return tryQueryStrings(query);
    }

    @Override
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesWhereStartEventNameEmpty() {
        final String query = this.cypherQueryCreator.rq8();
        return tryQueryStrings(query);
    }
}
