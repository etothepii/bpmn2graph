package org.etothepii.model.tinkerpop;

import com.arcadedb.ContextConfiguration;
import com.arcadedb.database.Database;
import com.arcadedb.database.DatabaseFactory;
import org.apache.tinkerpop.gremlin.arcadedb.structure.ArcadeGraph;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.driver.ser.GraphBinaryMessageSerializerV1;
import org.apache.tinkerpop.gremlin.driver.ser.Serializers;
import org.apache.tinkerpop.gremlin.process.traversal.IO;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import org.apache.tinkerpop.gremlin.util.Gremlin;
import org.etothepii.model.tinkerpop.DBConnectionType;
import org.etothepii.model.GraphDBName;
import org.janusgraph.core.JanusGraphFactory;

import javax.naming.OperationNotSupportedException;

import static org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource.traversal;

/**
 * A simple connector to connect to a graph DB using Tinkerpop.
 * Unfortunately, many databases didnt seem to work under Tinkerpop.
 * But this class can be used and extended in case the tinkerpop is used more heavily.
 */
public class SimpleGraphDBConnector {

    public Graph graph = null;
    public Cluster cluster = null;
    public GraphTraversalSource g = null;

    private final Cluster.Builder builder = Cluster.build();


    public SimpleGraphDBConnector(GraphDBName dbName, DBConnectionType connectionType) throws OperationNotSupportedException {
        System.out.println("Tinkerpop Gremlin version is " + Gremlin.version());

        if (dbName.equals(GraphDBName.Tinkergraph) && connectionType.equals(DBConnectionType.Inmemory)) {
            this.graph = TinkerGraph.open();
            this.g = this.graph.traversal();

        } else if (dbName.equals(GraphDBName.ArcadeDB) && connectionType.equals(DBConnectionType.Embedded)) {
            this.graph = ArcadeGraph.open("tmp/arcade");
            this.g = traversal().withEmbedded(graph);

        } else if(dbName.equals(GraphDBName.Tinkergraph) && connectionType.equals(DBConnectionType.Remote)) {

            // Remote connection to localhost (Gremlin Server).
            this.builder.addContactPoint("localhost");
            this.builder.port(8182);
            this.builder.serializer(Serializers.GRAPHBINARY_V1D0);
            this.cluster = builder.create();
            this.g = traversal().withRemote(DriverRemoteConnection.using(this.cluster));
            this.graph = g.getGraph();

        } else if(dbName.equals(GraphDBName.JanusGraph) && connectionType.equals(DBConnectionType.Embedded)) {
            JanusGraphFactory.Builder builder = JanusGraphFactory.build().set("storage.backend", "inmemory");
            this.graph = builder.open();
            this.g = traversal().withEmbedded(this.graph);
        } else {
            throw new OperationNotSupportedException("No connection available which supports this currently. Sorry.");
        }
    }

    public void export2GraphML(String filename) {
        this.g.io(filename).with(IO.writer, IO.graphml).write().iterate();
    }

    public void close() {
        if (this.cluster != null) {
            this.cluster.close();
        }
        if (this.g != null) {
            try {
                this.g.close();
            } catch(Exception e) {
                //
            }
        }
        if (this.graph != null){
            try {
                this.graph.close();
            } catch(Exception e) {
                //
            }
        }
    }
}
