package org.etothepii.model.tinkerpop;


/**
 * The type of connection Tinkerpop supports.
 */
public enum DBConnectionType {
    Remote,
    Embedded,
    Inmemory
}