package org.etothepii;

import org.apache.commons.lang.NotImplementedException;
import org.apache.shiro.crypto.hash.Hash;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.bpmn.instance.*;
import org.etothepii.model.*;
import org.etothepii.model.adapter.GraphDBJavaAdapter;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.joda.time.DateTime;

import java.io.File;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BPMNParser {

    /**
     * A list of adapters that can be used to access the databases
     */
    public List<? extends GraphDBJavaAdapter> adapters;

    public BPMNParser(List<? extends GraphDBJavaAdapter> adapters) {
        this.adapters = adapters;
    }


    /**
     * This method parses a given File, creates the given number of process instances and inserts (the model + the instances) into the databases
     * @param file the BPMN file to be parsed
     * @param numInstances the number of process instances to be create from a single model
     */
    public void parseBPMNAdd2Graph(File file, int numInstances) {
        this.createProcessModelGraphsFromBPMNFile(file, numInstances);
    }


    private void createProcessModelGraphsFromBPMNFile(File file, int numInstances) {

        //Use the Camunda Model API to read the XML file
        BpmnModelInstance model = Bpmn.readModelFromFile(file);

        // get processes that not necessarily have subprocesses, for simplicity.
        Set<Process> processes = model.getModelElementsByType(Process.class).stream().filter(p -> p.getFlowElements().stream().noneMatch(e -> e instanceof SubProcess)).collect(Collectors.toSet());

        // get meta info from the BPMN file
        Definitions defs = model.getDefinitions();
        System.out.println("Parsing bpmn file by " + defs.getExporter() + " " + defs.getExporterVersion() + " - " + file.getName());

        //Theoretically a BPMN file can define multiple processes. Even though in the thesis material we have exactly 1 process model per file.
        //We call a submethod to create a graph from the process. One time for the model and several times for the instances.
        for (Process p : processes) {
            String metanodeGid = this.createGraphFromProcess(defs, p, file.getName(), ProcessModelInstanceType.MODEL, "NONE");
            for (int i=0; i<numInstances; i++) {
                this.createGraphFromProcess(defs, p, file.getName(), ProcessModelInstanceType.INSTANCE, metanodeGid);
            }
        }
    }

    /**
     * Create a graph from the given process. Alternatively specify whether it is a model or instance and its instanceOfReference gid value of the meta node.
     * @return the gid of the created meta node.
     */
    private String createGraphFromProcess(Definitions definitions, Process p, String fileName) {
        return this.createGraphFromProcess(definitions, p, fileName, ProcessModelInstanceType.MODEL, "NONE");
    }
    private String createGraphFromProcess(Definitions defs, Process p, String fileName, ProcessModelInstanceType modelInstanceType, String instanceOfReferenceGid4MetaNode) {


        //For each process we create a UUID
        final String processUUID = UUID.randomUUID().toString();


        //TODO: What still could be done are annotations directly on edges / nodes.

        //we extract several types of elements.
        Set<Lane> lanes = p.getLaneSets().stream().flatMap(ls -> ls.getLanes().stream()).collect(Collectors.toSet());
        Set<FlowNode> nodes = p.getFlowElements().stream().filter(e -> e instanceof FlowNode).map(FlowNode.class::cast).collect(Collectors.toSet());
        //Set<FlowElement> edges = p.getFlowElements().stream().filter(e -> e instanceof MessageFlow || e instanceof SequenceFlow).map(FlowElement.class::cast).collect(Collectors.toSet());
        Set<FlowElement> dataNodes = p.getFlowElements().stream().filter(e -> e instanceof DataObjectReference || e instanceof  DataStoreReference).map(FlowElement.class::cast).collect(Collectors.toSet());
        Set<Triplet<? extends DataAssociation, String, String>> dataEdgesIds = new HashSet<>();
        Set<Pair<String, String>> boundaryAttachmentsFromGidToGid = new HashSet<>();


        //Construct a node containing meta information for the model
        //section meta node
        ModelPropertyMap metaProps = new ModelMetaNodePropertyMap(
                "Meta", processUUID + "_def", "Meta4" + (p.getName() != null ? defs.getName() : ""), modelInstanceType.toString(),
                defs.getExporter(), defs.getExporterVersion(), "Placeholder Process Description", "1.0.0", fileName
        );
        this.adapters.forEach(a -> a.addVertex(metaProps));

        //add "instanceOf" edge in case we intend to make this process graph an instance
        if (!instanceOfReferenceGid4MetaNode.equalsIgnoreCase("NONE")) {
            ModelPropertyMap metaInstanceOfProps = new ModelEdgePropertyMap(
                    "instanceOf", processUUID + "_defInstance", "instanceOf", ProcessModelInstanceType.INSTANCE.toString(), "instanceOf"
            );
            this.adapters.forEach(a -> a.addEdge(new Pair<>("gid", metaProps.get("gid")), new Pair<>("gid", instanceOfReferenceGid4MetaNode), metaInstanceOfProps));
        }

        //sift through data associations to save for later.
        //section data nodes
        for (FlowElement n : dataNodes) {
            if (n instanceof DataObjectReference || n instanceof DataStoreReference) {

                //get the corresponding lane if it exists.
                Optional<Lane> assocLane = lanes.stream().filter(l -> l.getFlowNodeRefs().contains(n)).findFirst();

                String label = n.getClass().getSimpleName().replace("Impl", "");
                String nodeType = "Data";
                ModelPropertyMap props = new ModelNodePropertyMap(
                        label, processUUID + n.getId(), n.getName() != null ? n.getName() : "", modelInstanceType.toString(), nodeType,
                        new DateTime().toString(), new DateTime().toString(), assocLane.isPresent() ? assocLane.get().getName() : "NONE", metaProps.get("gid")
                );

                this.adapters.forEach(a -> a.addVertex(props));
            }
        }

        //add flow nodes
        //section flow nodes
        for (FlowNode n : nodes) {

            //get the corresponding lane if it exists.
            Optional<Lane> assocLane = lanes.stream().filter(l -> l.getFlowNodeRefs().contains(n)).findFirst();


            String label = n.getClass().getSimpleName().replace("Impl", "");
            String nodeType = "NONE";
            if (label.contains("Task")) {
                nodeType = "Task";
            } else if (label.contains("Gateway")) {
                nodeType = "Gateway";
            } else if (label.contains("Event")) {
                nodeType = "Event";
            }
            ModelPropertyMap props = new ModelNodePropertyMap(
                    label, processUUID + n.getId(), n.getName() != null ? n.getName() : "", modelInstanceType.toString(), nodeType,
                    new DateTime().toString(), new DateTime().toString(), assocLane.isPresent() ? assocLane.get().getName() : "NONE", metaProps.get("gid")
            );


            this.adapters.forEach(a -> a.addVertex(props));

            if (n instanceof Activity) {
                //save data associations for later.
                Set<Pair<? extends DataAssociation, Optional<ItemAwareElement>>> potentialIns = ((Activity) n).getDataInputAssociations().stream().map(di -> new Pair<>(di, di.getSources().stream().findFirst())).collect(Collectors.toSet());
                Set<Pair<? extends DataAssociation, ItemAwareElement>> potentialOuts = ((Activity) n).getDataOutputAssociations().stream().map(o -> new Pair<>(o, o.getTarget())).collect(Collectors.toSet());
                dataEdgesIds.addAll(potentialIns.stream().filter(i -> i.getValue1().isPresent()).map(i -> new Triplet<>(i.getValue0(), processUUID + i.getValue1().get().getId(), props.get("gid"))).collect(Collectors.toSet()));
                dataEdgesIds.addAll(potentialOuts.stream().map(o -> new Triplet<>(o.getValue0(), props.get("gid"), processUUID + o.getValue1().getId())).collect(Collectors.toSet()));
            } else if (n instanceof BoundaryEvent) {
                //those events are "attached" to activities in the diagram. But are not connected over edges, so we create "epsilon" edges.
                Activity belongsTo = ((BoundaryEvent) n).getAttachedTo();
                if (belongsTo != null) {
                    boundaryAttachmentsFromGidToGid.add(new Pair<>(processUUID + belongsTo.getId(), props.get("gid")));
                }
            }
        }

        //add the saved data association edges
        //section data edges
        for (Triplet<? extends DataAssociation, String, String> dataEdgeTriplet : dataEdgesIds) {

            String label = dataEdgeTriplet.getValue0().getClass().getSimpleName().replace("Impl", "");
            ModelPropertyMap props = new ModelEdgePropertyMap(
                    label, dataEdgeTriplet.getValue0().getId(), dataEdgeTriplet.getValue0().getId(), modelInstanceType.toString(), "DataAssociation"
            );

            for (GraphDBJavaAdapter adapter : this.adapters) {
                adapter.addEdge(new Pair<>("gid", dataEdgeTriplet.getValue1()), new Pair<>("gid", dataEdgeTriplet.getValue2()), props);
            }

        }

        //add flow edges
        //section flow edges
        Set<FlowElement> edges = p.getFlowElements().stream().filter(e -> e instanceof MessageFlow || e instanceof SequenceFlow)
                .map(FlowElement.class::cast).collect(Collectors.toSet());
        for (FlowElement n : edges) {

            String label = n.getClass().getSimpleName().replace("Impl", "");
            ModelPropertyMap props = new ModelEdgePropertyMap(
                    label, processUUID + n.getId(), n.getName() != null ? n.getName() : "", modelInstanceType.toString(), "Flow"
            );

            for (GraphDBJavaAdapter adapter : this.adapters) {
                if (n instanceof SequenceFlow) {
                    adapter.addEdge(new Pair<>("gid", processUUID + ((SequenceFlow) n).getSource().getId()),
                            new Pair<>("gid", processUUID + ((SequenceFlow) n).getTarget().getId()), props);
                } else {
                    adapter.addEdge(new Pair<>("gid", processUUID + ((MessageFlow) n).getSource().getId()),
                            new Pair<>("gid", processUUID + ((MessageFlow) n).getTarget().getId()), props);
                }
            }
        }


        //section boundary event edges
        for (Pair<String, String> bound : boundaryAttachmentsFromGidToGid) {
            ModelPropertyMap props = new ModelEdgePropertyMap(
                    "boundaryEvent", processUUID + bound.getValue1() + "_epsilon", "Epsilon Edge", modelInstanceType.toString(), "Event"
            );

            for (GraphDBJavaAdapter adapter : this.adapters) {
                adapter.addEdge(new Pair<>("gid", bound.getValue0()), new Pair<>("gid", bound.getValue1()), props);
            }
        }


        //finally, add edges pointing from metanode towards start events
        //section meta edges
        for (StartEvent ev : nodes.stream().filter(n -> n instanceof StartEvent).map(StartEvent.class::cast).collect(Collectors.toSet())) {
            ModelPropertyMap props = new ModelEdgePropertyMap(
                    "startsAt", "metaEdge4" + processUUID + ev.getId(), "Meta Edge", modelInstanceType.toString(), "startsAt"
            );

            for (GraphDBJavaAdapter adapter : this.adapters) {
                adapter.addEdge(new Pair<>("gid", metaProps.get("gid")), new Pair<>("gid", processUUID + ev.getId()), props);
            }
        }

        //return the gid of the created metanode.
        return metaProps.get("gid");
    }
}
