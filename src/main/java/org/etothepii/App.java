package org.etothepii;


import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.etothepii.model.ModelNodePropertyMap;
import org.etothepii.model.adapter.*;
import org.etothepii.model.tinkerpop.DBConnectionType;
import org.etothepii.model.GraphDBName;
import org.etothepii.model.tinkerpop.SimpleGraphDBConnector;

import java.io.File;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.DoubleStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;


/**
 * The main program.
 *
 */
public class App 
{

    /**
     * The relative directory to the project, where the BPMN files are located.
     */
    public static final String PROCESS_FILES_DIRECTORY = "data\\BPMN20ModelsReprository";

    /**
     *
     * The main method to create database adapters, call the BPMN parser and measure runtimes of insert/Read queries.
     * @param args not relevant
     */
    public static void main( String[] args ) {

        List<GraphDBJavaAdapter> adapters = new LinkedList<>();
        final Path projDirectory = Path.of(System.getProperty("user.dir"));

        try {

            //Make references to all bpmn files from the given folder
            File inputDataDir = projDirectory.resolve(PROCESS_FILES_DIRECTORY).toFile();
            File[] bpmnFiles = Arrays.stream(inputDataDir.listFiles((dir, name) -> name.endsWith(".bpmn"))).toArray(File[]::new);

            /**
             *   initialize database adapters.
             *   The adapters try to create connections to the corresponding databases.
             *   You can add only a single adapter if you want to test just a single database.
             */
            //TinkerpopAdapter tpa = new TinkerpopAdapter(GraphDBName.Tinkergraph, DBConnectionType.Inmemory);
            //adapters.add(tpa);
            adapters.add(new Neo4jAdapter());
            //adapters.add(new OrientDBAdapter());
            //adapters.add(new ArcadeDBAdapter());
            //adapters.add(new RedisGraphAdapter());
            //adapters.add(new AgensGraphAdapter());

            /**
             * Here we evaluate the runtimes for the insert queries for the given bpmnfiles on all the adapters.
             * We pass the adapters, bpmn files and how many process instances we want per model.
             */
            EvaluateInsertQueriesOnAdaptersAndPrintRuntimes(adapters, bpmnFiles, 2);


            //optionally export to GraphML with the tinkerpop adapter
            //Almost Only Tinkerpop features GraphML export, so there is almost no way around using tinkerpop for that.
            //tpa.connector.export2GraphML(projDirectory + "/export/bpmn-output.graphml");


            /**
             * Here we evaluate the readqueries RQ1-RQ8 on all the adapters.
             */
            //EvaluateReadQueriesOnAdaptersAndPrintRuntimes(adapters);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            for (GraphDBJavaAdapter adapter : adapters) {
                if (adapter != null) {
                    adapter.close();
                }
            }
        }
    }


    /**
     * clear graph and let our parser parse the BPMN files.
     * We measure the time it took for the parser to parse all the files into all the given databases
     */
    private static void EvaluateInsertQueriesOnAdaptersAndPrintRuntimes(List<GraphDBJavaAdapter> adapters, File[] bpmnFiles, int numInstancesPerModel) {
        adapters.forEach(GraphDBJavaAdapter::clearDB);
        BPMNParser myBPMNParser = new BPMNParser(adapters);
        System.out.println("Starting execution with " + bpmnFiles.length + " process models.");
        long startTime = System.currentTimeMillis();
        for (File file : bpmnFiles) {
            try {
                myBPMNParser.parseBPMNAdd2Graph(file, numInstancesPerModel);
            } catch (Exception e) {
                System.out.println("Could not parse " + file.getName());
                e.printStackTrace();
            }
        }
        long duration = System.currentTimeMillis() - startTime;
        System.out.println("Took " + duration + " ms.");
    }


    private static void EvaluateReadQueriesOnAdaptersAndPrintRuntimes(List<GraphDBJavaAdapter> adapters) {
        EvaluateReadQueriesOnAdaptersAndPrintRuntimes(adapters, 30);
    }

    /**
     * /**
     * This method evaluates all the 8 read queries of the thesis on the given adapters.
     * It takes the average runtime of numRounds calls. By default that value is 30, as it is in the thesis.
     * @param adapters      The database adapters to test.
     * @param numRounds     The count of runtimes to measure the average of. To have a representative runtime of each adapter/database.
     */
    private static void EvaluateReadQueriesOnAdaptersAndPrintRuntimes(List<GraphDBJavaAdapter> adapters, int numRounds) {
        HashMap<RQName, Vector<Long>> timesOfAllRounds = new HashMap<>();
        Arrays.stream(RQName.values()).forEach(rqn -> timesOfAllRounds.put(rqn, new Vector<Long>()));
        for (int i=0; i<numRounds; i++) {
            HashMap<RQName, Long> timesOfThisRound = testFetchQueriesOnAdapter(((ThesisEvaluationFetchQueries) adapters.stream().filter(a -> a instanceof ThesisEvaluationFetchQueries).findFirst().get()));
            timesOfAllRounds.entrySet().forEach(e -> e.getValue().add(timesOfThisRound.get(e.getKey())));
        }
        for (RQName rqn : RQName.values()) {

            StandardDeviation stdDev = new StandardDeviation();
            Mean mean = new Mean();

            double stdDevResult = stdDev.evaluate(timesOfAllRounds.get(rqn).stream().mapToDouble(a -> a).toArray());
            double meanResult = mean.evaluate(timesOfAllRounds.get(rqn).stream().mapToDouble(a -> a).toArray());

            System.out.println(rqn.toString() + ": " + timesOfAllRounds.get(rqn)
                    + ", max=" + timesOfAllRounds.get(rqn).stream().mapToLong(a -> a).max().getAsLong()
                    + ", min=" + timesOfAllRounds.get(rqn).stream().mapToLong(a -> a).min().getAsLong()
                    + ", avg=" + Math.round(timesOfAllRounds.get(rqn).stream().mapToLong(a -> a).average().getAsDouble())
                    + ", mean=" + (Math.round(meanResult * 100.0) / 100.0)
                    + ", stdDev=" + (Math.round(stdDevResult * 100.0) / 100.0));
        }
    }


    /**
     * The list of Read Query Names to test.
     */
    public enum RQName {
        RQ1,
        RQ2,
        RQ3,
        RQ4,
        RQ5,
        RQ6,
        RQ7,
        RQ8
    }

    /**
     * This method evaluates all the read queries on a ThesisEvaluationFetchQueries object. Which is
     * For all the 8 read queries, the runtime is measured.
     * @param fetchQueriesImplementor an object implementing methods to evaluate the read queries.
     * @return a hashmap, indicating the runtime of the ThesisEvaluationFetchQueries-object for every ReadQuery RQ1-RQ8.
     */
    private static HashMap<RQName, Long> testFetchQueriesOnAdapter(ThesisEvaluationFetchQueries fetchQueriesImplementor) {
        HashMap<RQName, Long> timesTaken = new HashMap<RQName, Long>();
        long startTime, tookTime;

        startTime = System.currentTimeMillis();
        fetchQueriesImplementor.getCountOfParallelGateways();
        tookTime = (System.currentTimeMillis() - startTime);
        timesTaken.put(RQName.RQ1, tookTime);

        startTime = System.currentTimeMillis();
        fetchQueriesImplementor.getCountOfPattern2SuccessiveTasks();
        tookTime = (System.currentTimeMillis() - startTime);
        timesTaken.put(RQName.RQ2, tookTime);

        startTime = System.currentTimeMillis();
        fetchQueriesImplementor.getCountOfPatternStarteventTaskTaskGatewayTaskGateway();
        tookTime = (System.currentTimeMillis() - startTime);
        timesTaken.put(RQName.RQ3, tookTime);

        startTime = System.currentTimeMillis();
        fetchQueriesImplementor.getCountOfTasksWithDataInName();
        tookTime = (System.currentTimeMillis() - startTime);
        timesTaken.put(RQName.RQ4, tookTime);

        startTime = System.currentTimeMillis();
        fetchQueriesImplementor.getCountOfNodesWithMoreThanFiveOutgoingEdges();
        tookTime = (System.currentTimeMillis() - startTime);
        timesTaken.put(RQName.RQ5, tookTime);

        startTime = System.currentTimeMillis();
        fetchQueriesImplementor.getCountOfStartEventsConnectedToATaskOver7StepsMaximum();
        tookTime = (System.currentTimeMillis() - startTime);
        timesTaken.put(RQName.RQ6, tookTime);

        startTime = System.currentTimeMillis();
        Collection<String> idList7 = fetchQueriesImplementor.getIDsOfMetaNodesFromProcessInstancesFromCamundaModeler();
        tookTime = (System.currentTimeMillis() - startTime);
        timesTaken.put(RQName.RQ7, tookTime);

        startTime = System.currentTimeMillis();
        Collection<String> idList8 = fetchQueriesImplementor.getIDsOfMetaNodesFromProcessInstancesWhereStartEventNameEmpty();
        tookTime = (System.currentTimeMillis() - startTime);
        timesTaken.put(RQName.RQ8, tookTime);

        return timesTaken;
    }
}
