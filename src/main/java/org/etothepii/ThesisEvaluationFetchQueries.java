package org.etothepii;

import java.util.Collection;
import java.util.Enumeration;
import java.util.List;


/**
 * An interface exposing all the 8 different read queries of the evaluation in the thesis.
 */
public interface ThesisEvaluationFetchQueries {

    public int getCountOfParallelGateways();
    public int getCountOfPattern2SuccessiveTasks();
    public int getCountOfPatternStarteventTaskTaskGatewayTaskGateway();
    public int getCountOfTasksWithDataInName();
    public int getCountOfNodesWithMoreThanFiveOutgoingEdges();
    public int getCountOfStartEventsConnectedToATaskOver7StepsMaximum();
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesFromCamundaModeler();
    public Collection<String> getIDsOfMetaNodesFromProcessInstancesWhereStartEventNameEmpty();
}
